#!/bin/sh
echo 'Packaging native image'
mkdir -p target &&
chmod +x serverfunction &&
zip -j sensorsProjection.zip serverfunction src/main/native/* &&
mv sensorsProjection.zip target/ &&
ls -lh target/sensorsProjection.zip | awk '{print "packaged native "$9, "with size "$5}' &&
echo 'DONE: native image packaged'
