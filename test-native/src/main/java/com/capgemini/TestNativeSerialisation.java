package com.capgemini;

import static java.lang.String.format;

import java.io.IOException;
import java.io.InputStream;

import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.util.IOUtils;
import uk.capgemini.sensors.lambda.AWSDeserializer;

public class TestNativeSerialisation {

    public static String readFileFromClasspath(final String filename) {
        try (InputStream is = ClassLoader.getSystemResourceAsStream(filename)) {
            return IOUtils.toString(is);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    public static void throwIfNull(final Object o, final String objectName) {
        if (o == null) {
            throw new RuntimeException(format("% is null"));
        }
    }

    public static void throwIfEmpty(final String a, final String objectName) {
        throwIfNull(a, objectName);
        if ("".equals(a)) {
            throw new RuntimeException(format("% is empty"));
        }
    }

    public static void main(String[] args) throws Exception {
        String inputJson = readFileFromClasspath("ApproxCreationDate.json");
        throwIfEmpty(inputJson, "file");
        final DynamodbEvent event = AWSDeserializer.deserializeDynamoEvents(inputJson);
        System.out.println(event.getRecords().get(0).getDynamodb().getApproximateCreationDateTime());
    }
}
