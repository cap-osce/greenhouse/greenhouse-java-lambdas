package uk.capgemini.greenhouse;

public class SensorEvents {

    public static final String SENSOR_TEMPERATURE_READING_ACQUIRED = "sensor.temperature.reading-acquired";
    public static final String SENSOR_HUMIDITY_READING_ACQUIRED = "sensor.humidity.reading-acquired";
    public static final String SENSOR_MOISTURE_READING_ACQUIRED = "sensor.moisture.reading-acquired";
    public static final String SENSOR_LUMINOSITY_READING_ACQUIRED = "sensor.luminosity.reading-acquired";

}
