package uk.capgemini.greenhouse;

public class Measurement {

    public final static String TEMPERATURE = "TEMPERATURE";
    public final static String HUMIDITY = "HUMIDITY";
    public final static String MOISTURE = "MOISTURE";
    public final static String LUMINOSITY = "LUMINOSITY";

}
