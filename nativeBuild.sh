#!/usr/bin/env bash
TARGET_FILENAME=${2}
JAR=${1}
echo "Compiling Native: ${JAR}"
echo "Target file: ${TARGET_FILENAME}"
native-image --version
native-image \
    --allow-incomplete-classpath                                                    \
    --enable-http                                                                   \
    --enable-https                                                                  \
    --enable-url-protocols=http,https                                               \
    --enable-all-security-services                                                  \
    -H:Log=registerResource:basic                                                   \
    -Djava.net.preferIPv4Stack=true                                                 \
    -H:+ReportUnsupportedElementsAtRuntime                                          \
    -H:DynamicProxyConfigurationFiles=src/main/build/dynamic-proxies.json           \
    -H:ResourceConfigurationFiles=src/main/build/resources.json                     \
    -H:ReflectionConfigurationFiles=src/main/build/reflect.json                     \
    --static                                                                        \
    --no-server                                                                     \
    -jar ${JAR} \
    -H:Name=${TARGET_FILENAME}                                                          &&
 chmod +x serverfunction
