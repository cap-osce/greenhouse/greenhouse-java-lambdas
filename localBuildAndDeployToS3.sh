#!/usr/bin/env bash

if [ -z "$GREENHOUSE_PREFIX" ]; then
    echo "env variable greenhousePrefix is not defined."
    echo "use: export GREENHOUSE_PREFIX=greenhouse-<CHANGE_FOR_YOUR_NAME>"
    exit 1
fi

if [ "$GREENHOUSE_PREFIX" == "greenhouse" ]; then
    export RELEASE_TYPE="release"
  else
    export RELEASE_TYPE="snapshot"
fi

GRAAL_VERSION="1.0.0-rc16"
echo "Create docker vm with GraalVM with version ${GRAAL_VERSION}"
docker build --build-arg GRAAL_VERSION="${GRAAL_VERSION}" -t capgemini/graal-native-image:"${GRAAL_VERSION}" . &&
echo "Build Java packages" &&
mvn clean package &&
echo "Building native image with Graal version: ${GRAAL_VERSION}"
docker run -it -v /Users/joao/projects/greenhouse/greenhouse-java-lambdas/:/project \
    --rm capgemini/graal-native-image:${GRAAL_VERSION}                              \
    ./buildNativePackages.sh        &&
./packageNative.sh      &&
./deploy.sh             &&
echo "DONE: Build and Deployment to S3 complete"
