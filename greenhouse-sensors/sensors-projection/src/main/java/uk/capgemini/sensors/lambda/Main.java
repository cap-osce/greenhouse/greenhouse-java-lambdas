package uk.capgemini.sensors.lambda;

import static java.lang.String.format;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.amazonaws.services.lambda.runtime.ClientContext;
import com.amazonaws.services.lambda.runtime.CognitoIdentity;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.LambdaRuntime;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.util.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

public class Main implements Runnable {

    private static final String REQUEST_ID_HEADER = "Lambda-Runtime-Aws-Request-Id";

    private final URI nextMessageUrl;
    private final String responseUrlPattern;
    private final String initErrorUrl;
    private final String invocationErrorUrl;
    private final HttpClient client = HttpClientBuilder.create().build();
    private final UpdateProjection function = new UpdateProjection();

    private final Context context = new BasicContext(new NativeLogger());

    private Main(String rootUrl) {
        nextMessageUrl = URI.create(String.format("http://%s/2018-06-01/runtime/invocation/next", rootUrl));
        initErrorUrl = String.format("http://%s/2018-06-01/runtime/init/error", rootUrl);
        invocationErrorUrl = String.format("http://%s/2018-06-01/runtime/invocation/%%s/error", rootUrl);
        responseUrlPattern = String.format("http://%s/2018-06-01/runtime/invocation/%%s/response", rootUrl);
    }

    public static void main(String[] args) {
        System.out.println("Hello! got to the main");
        String url = System.getenv("AWS_LAMBDA_RUNTIME_API");
        new Main(url).run();
    }

    @Override
    @SuppressWarnings("squid:S2189")
    public void run() {
        boolean running = true;
        HttpGet request = new HttpGet(nextMessageUrl);
        while (running) {
            HttpResponse message = null;
            try {
                System.out.println("Loop");
                message = client.execute(request);
                System.out.println("Response : " + message.getStatusLine().getStatusCode());
                if (message.getStatusLine().getStatusCode() == 200) {


                    System.out.println("Has 200 - parsing");

                    printHeader(message, "lambda-runtime-client-context");

                    final String data = parseRequest(message);
                    System.out.println(data);

                    final DynamodbEvent event = AWSDeserializer.deserializeDynamoEvents(data);

                    function.handleRequest(event, context);

                    sendResponse(message, "OK");
                } else {
                    message.getEntity().getContent().close();
                }
                System.out.println("OK Loop");
            } catch (IOException e) {
                e.printStackTrace();
                sendErrorMessage(message, e.getMessage());
            } catch (Throwable e) {
                e.printStackTrace();
                sendErrorMessage(message, e.getMessage());
            }
            request.reset();
            System.out.println("End Loop");
        }
        System.out.println("Exit Loop");
    }

    private void printHeader(final HttpResponse message, final String headerName) {
        final Header lambdaRuntimeClientContext = message.getFirstHeader(headerName);
        if (lambdaRuntimeClientContext != null) {
            System.out.println(format("Header: %s - %s", lambdaRuntimeClientContext.getName(), lambdaRuntimeClientContext.getValue()));
        } else {
            System.out.println(format("No %s header found", headerName));
        }
    }

    private void sendErrorMessage(final HttpResponse message, final String errorString) {
        if (message != null) {
            sendInvocationErrorResponse(message, errorString);
        }
    }

    private String parseRequest(HttpResponse response) throws IOException {
        try (InputStream stream = response.getEntity().getContent()) {
            return IOUtils.toString(stream);
        }
    }

    private void sendResponse(HttpResponse request, String body) throws IOException {
        String url = String.format(responseUrlPattern, request.getFirstHeader(REQUEST_ID_HEADER).getValue());
        System.out.println("Sending to " + url);
        HttpPost request1 = new HttpPost(url);
        request1.setEntity(new StringEntity(body));
        HttpResponse response = client.execute(request1);
        request1.reset();
        System.out.println("Response: " + response.getStatusLine().getStatusCode() + " : " + response.getStatusLine().getReasonPhrase());
    }

    private void sendInvocationErrorResponse(HttpResponse request, String body) {
        String url = String.format(invocationErrorUrl, request.getFirstHeader(REQUEST_ID_HEADER).getValue());
        System.out.println("Sending error to " + url);
        HttpPost request1 = new HttpPost(url);
        try {
            request1.setEntity(new StringEntity(body));
            HttpResponse response = client.execute(request1);
            request1.reset();
            System.out.println("Response: " + response.getStatusLine().getStatusCode() + " : " + response.getStatusLine().getReasonPhrase());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
    }

    public class NativeLogger implements LambdaLogger{

        @Override
        public void log(final String s) {
            System.out.println(s);
        }

        @Override
        public void log(final byte[] bytes) {
            for (byte b: bytes) {
                System.out.print(Integer.toHexString(b));
                System.out.print(", ");
            }
            System.out.println();
        }
    }

    public class BasicContext implements Context {

        private final LambdaLogger logger;

        public BasicContext(final LambdaLogger logger) {
            this.logger = logger;
        }

        @Override
        public String getAwsRequestId() {
            return null;
        }

        @Override
        public String getLogGroupName() {
            return null;
        }

        @Override
        public String getLogStreamName() {
            return null;
        }

        @Override
        public String getFunctionName() {
            return null;
        }

        @Override
        public String getFunctionVersion() {
            return null;
        }

        @Override
        public String getInvokedFunctionArn() {
            return null;
        }

        @Override
        public CognitoIdentity getIdentity() {
            return null;
        }

        @Override
        public ClientContext getClientContext() {
            return null;
        }

        @Override
        public int getRemainingTimeInMillis() {
            return 0;
        }

        @Override
        public int getMemoryLimitInMB() {
            return 0;
        }

        @Override
        public LambdaLogger getLogger() {
            return this.logger;
        }
    }
}
