package uk.capgemini.sensors.lambda;

import static java.lang.String.format;

import java.util.HashMap;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemResult;
import com.amazonaws.services.lambda.runtime.LambdaLogger;

public class DynamoDBHelper {

    private final String awsRegion;
    private final LambdaLogger logger;
    private final AmazonDynamoDB client;

    public DynamoDBHelper(final String awsRegion, final LambdaLogger logger) {
        this.awsRegion = awsRegion;
        this.logger = logger;
        this.client = AmazonDynamoDBClientBuilder.standard().withRegion(awsRegion).build();
    }

    public void putItem(final String tableName, final HashMap<String, AttributeValue> itemValues) {

        try {
            final PutItemResult putItemResult = client.putItem(tableName, itemValues);
            logger.log(putItemResult.toString());
        } catch (Throwable e) {
            final String errorMessage = format("Error: The table \"%s\" can't be found.\n", tableName);
            logger.log(errorMessage);
            throw new RuntimeException(errorMessage, e);
        }
    }
}
