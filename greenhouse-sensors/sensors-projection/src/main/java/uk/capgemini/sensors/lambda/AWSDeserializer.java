package uk.capgemini.sensors.lambda;

import java.io.IOException;
import java.time.Instant;
import java.util.Date;

import com.amazonaws.services.dynamodbv2.model.OperationType;
import com.amazonaws.services.dynamodbv2.model.Record;
import com.amazonaws.services.dynamodbv2.model.StreamRecord;
import com.amazonaws.services.dynamodbv2.model.StreamViewType;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

public class AWSDeserializer {


    public interface StreamRecordInstantSerialiser {
        @JsonDeserialize(using = UnixEpochToDate.class)
        void setApproximateCreationDateTime(Date approximateCreationDateTime);

    }

    public static class UnixEpochToDate extends StdDeserializer<Date> {

        public UnixEpochToDate() {
            this(null);
        }

        public UnixEpochToDate(final Class<?> vc) {
            super(vc);
        }

        @Override
        public Date deserialize(final JsonParser jsonParser, final DeserializationContext deserializationContext) throws IOException, JsonProcessingException {
            final Instant instant = Instant.ofEpochSecond(jsonParser.getLongValue());
            return Date.from(instant);
        }
    }

    public static class DateToUnixEpoch extends StdSerializer<Date> {

        public DateToUnixEpoch() {
            this(null);
        }

        public DateToUnixEpoch(final Class<Date> t) {
            super(t);
        }

        @Override
        public void serialize(final Date date, final JsonGenerator jsonGenerator, final SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeNumber(date.toInstant().getEpochSecond());
        }
    }

    public interface RecordIgnoreDuplicateMethods {
        @JsonIgnore
        void setEventName(OperationType eventName);

        @JsonProperty("eventName")
        void setEventName(String eventName);
    }

    public interface StreamRecordIgnoreDuplicateMethods {
        @JsonIgnore
        void setStreamViewType(StreamViewType streamViewType);

        @JsonProperty("StreamViewType")
        void setStreamViewType(String streamViewType);
    }

    public static class PropertyNamingFix extends PropertyNamingStrategy.PropertyNamingStrategyBase {
        @Override
        public String translate(String propertyName) {
            switch (propertyName) {
                case "eventID":
                    return "eventID";
                case "ApproximateCreationDateTime":
                    return "approximateCreationDateTime";
                case "eventVersion":
                    return "eventVersion";
                case "eventSource":
                    return "eventSource";
                case "awsRegion":
                    return "awsRegion";
                case "dynamodb":
                    return "dynamodb";
                case "eventSourceARN":
                    return "eventSourceARN";
                default:
                    String first = propertyName.substring(0, 1);
                    String rest = propertyName.substring(1);
                    return first.toUpperCase() + rest;
            }
        }
    }


    public static DynamodbEvent deserializeDynamoEvents(String json) throws IOException {
        final ObjectMapper objectMapper = new ObjectMapper();

        objectMapper.configure(JsonParser.Feature.ALLOW_COMMENTS, true);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        objectMapper.addMixIn(Record.class, RecordIgnoreDuplicateMethods.class);
        objectMapper.addMixIn(StreamRecord.class, StreamRecordIgnoreDuplicateMethods.class);
        objectMapper.addMixIn(StreamRecord.class, StreamRecordInstantSerialiser.class);
        objectMapper.setPropertyNamingStrategy(new PropertyNamingFix());

        return objectMapper.readValue(json, DynamodbEvent.class);
    }


}