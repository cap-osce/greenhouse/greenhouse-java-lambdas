package uk.capgemini.sensors.lambda;

public class DynamoEvent {
    public static final String DYNAMO_DB_EVENT_INSERT = "INSERT";
    public static final String DYNAMO_DB_EVENT_UPDATE = "UPDATE";
}
