package uk.capgemini.sensors.lambda;

import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static uk.capgemini.sensors.lambda.DynamoEvent.DYNAMO_DB_EVENT_INSERT;

import java.util.HashMap;

import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import uk.capgemini.greenhouse.Measurement;
import uk.capgemini.greenhouse.SensorEvents;

public class UpdateProjection implements RequestHandler<DynamodbEvent, Void> {

    private DynamoDBHelper dynamoDBHelper = null;
    private String tableName;
    private LambdaLogger logger;


    @Override
    public Void handleRequest(final DynamodbEvent event, final Context context) {
        logger = context.getLogger();
        logger.log("Java Projection lambda running");

        final String prefix = System.getenv("prefix");
        final String awsRegion = System.getenv("awsRegion");

        tableName = new StringBuilder().append(prefix).append("-sensor-projection").toString();

        logger.log(format("Projection lambda handling %s records", event.getRecords().size()));

        dynamoDBHelper = new DynamoDBHelper(awsRegion, logger);


        for (final DynamodbEvent.DynamodbStreamRecord record : event.getRecords()) {

            if (record.getEventName() == null || DYNAMO_DB_EVENT_INSERT.equals(record.getEventName())
                    && record.getDynamodb() != null && record.getDynamodb().getNewImage() != null) {
                final HashMap<String, AttributeValue> item = new HashMap(record.getDynamodb().getNewImage());

                final AttributeValue type = item.get("Type");
                final AttributeValue value = item.get("Value");
                if (isNotEmpty(type.getS())
                        && isNotEmpty(value.getN())) {

                    switch (type.getS()) {
                        case SensorEvents.SENSOR_TEMPERATURE_READING_ACQUIRED:
                            updatePhysicalMeasure(item, new AttributeValue(Measurement.TEMPERATURE));
                            break;
                        case SensorEvents.SENSOR_HUMIDITY_READING_ACQUIRED:
                            updatePhysicalMeasure(item, new AttributeValue(Measurement.HUMIDITY));
                            break;
                        case SensorEvents.SENSOR_LUMINOSITY_READING_ACQUIRED:
                            updatePhysicalMeasure(item, new AttributeValue(Measurement.LUMINOSITY));
                            break;
                        case SensorEvents.SENSOR_MOISTURE_READING_ACQUIRED:
                            updatePhysicalMeasure(item, new AttributeValue(Measurement.MOISTURE));
                            break;
                        default:
                            logger.log(format("not interested in the event %s", type.getS()));
                    }
                } else {
                    logger.log("either the type or the value is empty");
                }


            } else {
                logger.log(format("Skipping dynamodb event %s", record.getEventName()));
            }
        }
        return null;
    }


    private void updatePhysicalMeasure(final HashMap<String, AttributeValue> item, final AttributeValue attributeValue) {
        item.put("Type", attributeValue);
        logger.log(format("Updating table %s", this.tableName));
        logger.log(format("Source %s", item.get("Source").getS()));
        logger.log(format("Type %s", item.get("Type").getS()));
        logger.log(format("Timestamp %s", item.get("Timestamp").getN()));
        logger.log(format("Value %s", item.get("Value").getN()));
        dynamoDBHelper.putItem(this.tableName, item);
    }
}
