package uk.capgemini.sensors.lambda;

import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.Date;

import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.util.IOUtils;
import org.junit.Test;

public class PojoTest {

    @Test
    public void shouldDeserialise() throws Exception {
        String inputJson = readFileFromClasspath("dynamoDBEvent.json");
        assertThat(isNotEmpty(inputJson), is(true));
        final DynamodbEvent event = AWSDeserializer.deserializeDynamoEvents(inputJson);

        assertThat(event.getRecords().size(), is(1));
        final DynamodbEvent.DynamodbStreamRecord ddbStreamRecord = event.getRecords().get(0);
        assertThat(ddbStreamRecord.getEventID(), is("f1fbbcf28d1e0073488dc4b87c51f705"));
        assertThat(ddbStreamRecord.getEventVersion(), is("1.1"));
        assertThat(ddbStreamRecord.getEventName(), is("INSERT"));
        assertThat(ddbStreamRecord.getDynamodb().getNewImage().get("Type").getS(), is("sensor.temperature.reading-acquired"));
    }

    @Test
    public void shouldDeserialiseDateTime() throws Exception {
        String inputJson = readFileFromClasspath("ApproxCreationDate.json");
        assertThat(isNotEmpty(inputJson), is(true));
        final DynamodbEvent event = AWSDeserializer.deserializeDynamoEvents(inputJson);

        assertThat(event.getRecords().size(), is(1));
        final DynamodbEvent.DynamodbStreamRecord ddbStreamRecord = event.getRecords().get(0);
        assertThat(ddbStreamRecord.getEventName(), is("INSERT"));
        final Date approximateCreationDateTime = ddbStreamRecord.getDynamodb().getApproximateCreationDateTime();
        assertThat(approximateCreationDateTime.toInstant(), is(Instant.ofEpochSecond(1556741132L)));
    }

    public String readFileFromClasspath(final String filename) {
        try (InputStream is = getClass().getClassLoader().getResourceAsStream(filename)) {
            return IOUtils.toString(is);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
