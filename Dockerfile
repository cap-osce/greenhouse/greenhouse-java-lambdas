ARG GRAAL_VERSION
FROM oracle/graalvm-ce:${GRAAL_VERSION}

VOLUME /project
WORKDIR /project

ADD nativeBuild.sh nativeBuild.sh
ADD buildNativePackages.sh buildNativePackages.sh

ENTRYPOINT ["/bin/bash", "-c"]