#!/bin/sh


if [ -z "$GREENHOUSE_PREFIX" ]; then
    echo "env variable greenhousePrefix is not defined."
    echo "use: export GREENHOUSE_PREFIX=greenhouse-<CHANGE_FOR_YOUR_NAME>"
    exit 1
fi

if [ "$GREENHOUSE_PREFIX" == "greenhouse" ]; then
    export RELEASE_TYPE="release"
  else
    export RELEASE_TYPE="snapshot"
fi

echo "Copying to target: s3://greenhouse-lambdas/${RELEASE_TYPE}/${GREENHOUSE_PREFIX}/native/sensorsProjection.zip" &&
aws s3 cp target/sensorsProjection.zip s3://greenhouse-lambdas/${RELEASE_TYPE}/${GREENHOUSE_PREFIX}/native/sensorsProjection.zip &&
echo "DONE: Copied to S3"
